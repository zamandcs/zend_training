<?php
    include('template/conf.php');
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- bootstrap css-->
        <!-- <link rel="stylesheet" href="css/bootstrap/bootstrap.css" type="text/css"> -->

        <!-- Fontawesome -->
        <!-- <link rel="stylesheet" href="css/fontawesome/fontawesome.css" type="text/css"> -->

        <!-- Main css -->
        <link rel="stylesheet" href="css/main.css" type="text/css">
    </head>
    <body>
        <header class="container-fluid header">
            <div class="row">
                <div class="col-6 logo_container">  
                <img src="images/logo.png" alt="" class="img_fluid logo">                  
                    <h1 class="logo_text"> Zend Training</h1>
                </div>
                <div class="col-6 logo_container">
                    <i class="fa fa-twitter"></i><span class=""></span>
                    <i class="fa fa-instagram"></i><span class=""></span>
                    <i class="fa fa-facebook"></i><span class=""></span>
                    <i class="fa fa-youtube-play"></i><span class=""></span>
                </div>
            </div>
            
        </header>
        <nav class="navbar navbar-expand-lg navbar-success bg-success">
            <?php include("template/menu.php");?>
        </nav>
        <main class="container_fluid">
            <?php include("template/controller.php");?>
        </main>
        <footer class="footer">
           <span> Copyright @ Bionic System. All rights reserved <?php date("Y");?></span>
        </footer>
    </body>
</html>
<?php mysqli_close($con);?>